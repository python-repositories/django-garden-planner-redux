# Garden Planner App

Welcome to the Garden Planner App, a Django-based web application designed for home gardeners to optimize plant placement based on sunlight requirements and garden dimensions. This app provides an intuitive interface for garden planning, considering various factors like plant characteristics, sunlight patterns, and garden shapes.

## Table of Contents
[[_TOC_]]

## Features
- **Garden Layout Design**: Draw or input garden dimensions with predefined shapes.
- **Sunlight Calculation**: Optimize plant placement based on real-time sunlight data.
- **Plant Database**: Comprehensive plant information, including growth patterns and companion planting.
- **User-Friendly Interface**: Designed for home gardeners with basic tech skills.
- **OAuth Integration**: Secure login using Google or other providers.

### Scope and Features

<details>
<summary>Click to view the detailed scope and features of the Garden Planner App</summary>

#### 1. Purpose of the App
The Garden Planner App is designed to assist home gardeners in planning their garden layouts, optimizing plant placement based on sunlight requirements, and understanding plant growth patterns. The app targets users with basic technical proficiency, aiming to simplify the garden planning process through an intuitive web interface.

#### 2. Key Features
- **Garden Layout Design**: Users can input or draw their garden dimensions, including irregular shapes. The app will offer predefined garden shapes for ease of use.
- **Plant Database**: A comprehensive database containing information about various plants, including sunlight and shade requirements, growth radius, height, and companion planting suggestions.
- **Sunlight Calculation Algorithm**: Based on user’s location (entered manually or detected via geolocation in future updates), the app will calculate the optimal plant placement, considering the daily sunlight exposure.
- **Intuitive User Interface**: A simple, user-friendly interface allowing users to easily interact with the app without requiring advanced technical skills.
- **User Accounts and Authentication**: Secure login using OAuth, with options like Google for authentication.
- **Responsive Design**: The app will be accessible on various devices, ensuring a consistent experience across desktops, tablets, and smartphones.

#### 3. Advanced Features (Future Scope)
- **3D Garden Visualization**: An enhanced feature to view the garden layout in 3D, providing a more realistic and detailed perspective.
- **Automated Location Detection**: Integration with location services to automatically detect the user’s geographical position.
- **Seasonal Planting Suggestions**: Recommendations for planting and replanting crops based on seasonal changes specific to the user’s location.

#### 4. Technical Specifications
- **Framework**: Django (Python 3.11+).
- **Database**: PostgreSQL for managing user data and plant information.
- **Frontend**: HTML, CSS, JavaScript, and Django Templates.
- **Hosting and Deployment**: Planned containerization using Docker, with deployment on Kubernetes or ECS.

#### 5. Limitations
- **Initial Version Constraints**: The first release will focus on manual entry of location data and 2D garden visualization.
- **Geographical Limitations**: Initially, the sunlight calculation might be less accurate for regions outside of major cities or with unique microclimates.
</details>

### Setting Up Postgres Locally
<details>
<summary>Setting up Postgres DBs locally for work</summary>

```shell
#!/bin/bash

# PostgreSQL configuration script for Garden Planner App

# Database variables
DB_NAME="gardenplanner_dev"
DB_USER="gardenplanner_user"
DB_PASS="<YOUR_DEV_PASSWORD>" # Change this password

# Check for PostgreSQL installation
if ! command -v psql > /dev/null; then
    echo "PostgreSQL is not installed. Please install it first."
    exit 1
fi

# Create database
echo "Creating database: $DB_NAME"
createdb $DB_NAME

# Create user and assign password
echo "Creating user: $DB_USER"
psql -c "CREATE USER $DB_USER WITH PASSWORD '$DB_PASS';"

# Grant privileges to user
echo "Granting privileges to user $DB_USER on database $DB_NAME"
psql -c "GRANT ALL PRIVILEGES ON DATABASE $DB_NAME TO $DB_USER;"

echo "PostgreSQL configuration complete."

```

</details>

## Getting Started

### Prerequisites
- Python 3.11+
- Django
- Pipenv
- PostgreSQL

### Installation
1. **Clone the repository**: 
   ```
   git clone [repository URL]
   ```
2. **Set up the virtual environment**:
   ```
   pipenv shell
   ```
3. **Install dependencies**:
   ```
   pipenv install
   ```
4. **Configure the database**:
   ```
   [Instructions for setting up the PostgreSQL database]
   ```

## Usage
Detailed steps on how to use the application, including:
- Setting up your garden layout.
- Selecting plants from the database.
- Viewing sunlight recommendations.

## Contributing to Plant Database
Instructions on how users can contribute to the plant database, including moderation and validation processes.

## Deployment
Guidelines for deploying the app using GitLab pipelines, including steps for containerization and orchestration with Kubernetes or ECS.

## Security and Authentication
- Details about OAuth integration for secure user authentication.
- Information on how the app manages sensitive data.

## Feedback and Support
Information on how users can provide feedback or get support, including contact details or links to support channels.

## Versioning
Explanation of the versioning system used for the application.

## Authors
- **Your Name**
- Other contributors (if any)

## License
Details about the license of the application, possibly a link to the full license text.